import * as pulumi from "@pulumi/pulumi";
import * as azure from "@pulumi/azure";

const env = pulumi.getStack();
const config = new pulumi.Config();

const proj_identifier = config.require("proj-identifier");
const proj_identifier_fq = proj_identifier.concat("-").concat(env)

// Create an Azure Resource Group
const resourceGroup = new azure.core.ResourceGroup("rg-".concat(proj_identifier_fq));

// Create an Azure resource (Storage Account)
const account = new azure.storage.Account("sa", {
    // The location for the storage account will be derived automatically from the resource group.
    resourceGroupName: resourceGroup.name,
    accountTier: "Standard",
    accountReplicationType: "LRS",
});

const account_sas = pulumi.all([account.primaryConnectionString]).apply(([primaryConnectionString]) => azure.storage.getAccountSAS({
    connectionString: primaryConnectionString,
    start: "2020-06-23",
    expiry: "2020-06-26",
    permissions: {
        read: true,
        add: true,
        create: true,
        write: true,
        delete: true,
        list: true,
        process: true,
        update: true
    },
    resourceTypes: {
        container: true,
        object: true,
        service: true
    },
    services: {
        queue: true,
        blob: false,
        file: false,
        table: false
    },
    httpsOnly: true
}));

// Create an Azure Queue storage queue
const queue = new azure.storage.Queue("q-".concat(proj_identifier_fq), {
    storageAccountName: account.name,
});

// Export the connection string for the queue
export const queueURI = queue.id;
export const sasUrlQueryString = account_sas.sas;
