import { AzureFunction, Context } from "@azure/functions"
import * as storage from "azure-storage"

const timerTrigger: AzureFunction = async function (context: Context, myTimer: any): Promise<void> {
    const storageAccountName = process.env["storageAccountName"];
    const storageAccountKey = process.env["storageAccountKey"];
    const queueName = process.env["queue_name"];
    const randomMin: number = +process.env["random_min"];
    const randomMax: number = +process.env["random_max"];
    const randomNumber = Math.floor(Math.random() * (randomMax - randomMin + 1)) + randomMin;
    console.log("Random number was computed as " + randomNumber);

    var queueSvc = storage.createQueueService(storageAccountName, storageAccountKey);
    queueSvc.createQueueIfNotExists(queueName, function(error, results, response){
        if(!error){
          // Queue created or exists
        }
    });

    for (var _i = randomMin; _i <= randomNumber; _i++) {
        queueSvc.createMessage(queueName, "Yo sis is a test", function(error, results, response){
            if(!error){
                
            }
        });
    }
    console.log(randomNumber + " messages inserted.");

    if (myTimer.isPastDue)
    {
        context.log('Timer function is running late!');
    }
    context.log('Timer trigger function ran!');   

    context.done();
};

export default timerTrigger;
